import { useRef, useState } from "react";

export default function Document({ title, content }) {
  const [enabled, setEnabled] = useState(false);
  const contentRef = useRef(null);

  const onScroll = () => {
    if (contentRef?.current && !enabled) {
      const { scrollTop, scrollHeight, clientHeight } = contentRef?.current;
      if (scrollTop + clientHeight === scrollHeight) {
        setEnabled(true);
      }
    }
  };

  return (
    <div>
      <h1 className="title">{title}</h1>
      <article className="box is-medium is-dark">
        <div
          ref={contentRef}
          onScroll={onScroll}
          className="content"
          dangerouslySetInnerHTML={{ __html: content }}
        />
      </article>
      <div className="buttons">
        <button
          className="button is-primary is-light is-medium is-fullwidth"
          disabled={!enabled}
        >
          I Agree
        </button>
      </div>
    </div>
  );
}
