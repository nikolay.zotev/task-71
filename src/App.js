import "./App.css";

import { useEffect, useState } from "react";

import Document from "./Document";
import marked from "marked";

function App() {
  const [content, setContent] = useState(null);

  useEffect(() => {
    const params = new URLSearchParams();
    params.append("no-code", "true");
    params.append("num-blocks", "50");

    fetch(
      `https://jaspervdj.be/lorem-markdownum/markdown.txt?${params.toString()}`
    )
      .then((response) => response.text())
      .then((content) => {
        setContent(marked(content));
      })
      .catch(console.error);
  }, []);

  return (
    <div className="App">
      <div className="container is-max-desktop">
        <Document title="Terms and Conditions" content={content} />
      </div>
    </div>
  );
}

export default App;
